//
//  FeaturedViewController.swift
//  charity
//
//  Created by Doug Chisholm on 23/05/2019.
//  Copyright © 2019 Rippll. All rights reserved.
//

import UIKit
import MapKit

class FeaturedViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initMap()
        addPins()
    }
    
    @IBAction func checkShop() {
        NotificationCenter.default.post(name: Notification.Name("showTopLevelAlert"), object: nil)
    }
    
    func addPins() {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 51.5243386, longitude: -0.087609)
        annotation.title = "Visited 21 May"
        self.map.addAnnotation(annotation)
        
        let annotation2 = MKPointAnnotation()
        annotation2.coordinate = CLLocationCoordinate2D(latitude: 51.5262398, longitude: -0.0792064)
        annotation2.title = "Visited 20 May"
        self.map.addAnnotation(annotation2)
    }
    
    func updateMap() {
        //var latLon = Helper.getUserDefaultsString(name: "lastLatLon")
        var latLon = "0,0"
        if (!latLon.contains(",")) {
            latLon = "0,0"
        }
        let full : [String] = latLon.components(separatedBy: ",")
        
        var lat : String = full[0]
        var lon : String = full[1]
        
        let initialLocation = CLLocation(latitude: Double(lat)!, longitude: Double(lon)!)
        
        let regionRadius: CLLocationDistance = 300
        func centerMapOnLocation(location: CLLocation) {
            let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                      latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
            self.map.setRegion(coordinateRegion, animated: true)
        }
        
        centerMapOnLocation(location: initialLocation)
        self.map.showsUserLocation = true
    }
    
    func initMap() {
        let initialLocation = CLLocation(latitude: 51.1, longitude: -0.1)
        
        let regionRadius: CLLocationDistance = 300
        
        centerMapOnLocation(location: initialLocation)
        
        self.map.showsUserLocation = true
        self.map.userTrackingMode = .follow
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: 300, longitudinalMeters: 300)
        self.map.setRegion(coordinateRegion, animated: true)
    }
}
