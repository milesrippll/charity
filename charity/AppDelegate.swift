//
//  AppDelegate.swift
//  charity
//
//  Created by Doug Chisholm on 23/05/2019.
//  Copyright © 2019 Rippll. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import Foundation

typealias JSONStandard = [String: AnyObject]

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var justLogged = false
    var window: UIWindow?
    var locationManager = CLLocationManager()
    var lastLocation : CLLocation? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // showTopLevelAlert
        NotificationCenter.default.addObserver(self, selector: #selector(self.showTopLevelAlert(notification:)), name: Notification.Name("showTopLevelAlert"), object: nil)
        

        
        if (self.hasInUseLocation()) {
            if (!self.hasAlwaysLocation()) {
                self.setUpBackgroundLocationManager()
            }
        } else {
            self.setUpInitialLocationManager()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        locationManager.stopUpdatingLocation()
        /*
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            
            break
            
        case .restricted, .denied:
            
            break
            
        case .authorizedAlways:
            // Enable location features
            self.setUpBackgroundLocationManager()
            
            break
        case .authorizedWhenInUse:
            
            break
        }
 */
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        if (hasInUseLocation() || hasAlwaysLocation()) {
            locationManager.startUpdatingLocation()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func setUpInitialLocationManager () {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 1
        
        locationManager.startUpdatingLocation()
    }
    
    func setUpBackgroundLocationManager () {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy =  kCLLocationAccuracyHundredMeters //kCLLocationAccuracyBest
        
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        
        locationManager.pausesLocationUpdatesAutomatically = false
        
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 100 // 1
    }
    
    func hasInUseLocation() -> Bool {
        switch CLLocationManager.authorizationStatus() {
          
        case .authorizedWhenInUse:
            return true
            
        case .authorizedAlways:
            return false
   
        case .notDetermined:
            return false
            
            
        case .restricted, .denied:
            return false
            
        }
        
        return false
        
    }
    
    func hasAlwaysLocation() -> Bool {
        switch CLLocationManager.authorizationStatus() {
            
        case .authorizedAlways:
            return true
            
        case .authorizedWhenInUse:
            return false
            
        case .notDetermined:
            return false
            
            
        case .restricted, .denied:
            return false
            
        }
        
        return false
        
    }
    
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            
            break
            
        case .authorizedWhenInUse:
            
            if (!self.hasAlwaysLocation()) {
              // OFF  self.setUpBackgroundLocationManager()
            }
            //GeowaveService.TrackOnboardDetails(optedInAt: 100)
            
            break
            
        case .authorizedAlways:
            
            //GeowaveService.TrackOnboardDetails(optedInAt: 101)
            break
            
        case .notDetermined:
            break
        }
    }
    
    func getCurrentTimeStamp() -> String {
        let now = Date()
        
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = formatter.string(from: now)
        
        return dateString
    }
    
    @objc func showTopLevelAlert(notification: Notification) {
        
        var lastPlaceVisited = UserDefaults.standard.object(forKey: "lastPlaceVisited")
        if (lastPlaceVisited == nil) {
            lastPlaceVisited = "?"
        }
        
        let alertController = UIAlertController (title: "We think you are in:", message: lastPlaceVisited as! String, preferredStyle: .alert)
        
        let firstAction = UIAlertAction(title: "First", style: .default, handler: nil)
      //  alertController.addAction(firstAction)
        
        let cancelAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    func setAlertNotif(placeName : String) {
        let content = UNMutableNotificationContent()
        content.sound = UNNotificationSound.default
        
        let center = UNUserNotificationCenter.current()
        
        content.title = "Are you in:"
        
        content.body = placeName
      
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 300,
                                                        repeats: false)

        let identifier = "MyForecastLocalNotification" // This isnt unique yet so it will override the last one
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {
                
            }
        })
        
    }
    
    
    static func googlePlaces(latLon : String, completion: @escaping (String)->()) {
        
        var thelatLon = "51.5267272,-0.0840003" // Sains
       // thelatLon = "51.5244945,-0.0766334" // Dish
       thelatLon = latLon
        let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=10&key=AIzaSyCOMYmJ87krUcmAYulDb15u6wRw4Jxewvo&location=" + thelatLon
        
        let request = URLRequest(url:URL(string: url)!)  //create a url request
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if let data = data {
                do {
                    print("another call")
                    
                        do{
                            var readableJSON = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! JSONStandard
                            
                            
                            if let results = readableJSON["results"] as? [JSONStandard]{
                                print("JSON: ",results)
                                for i in 0..<results.count{
                                    
                                    let item = results[i]
                                    let name = item["name"] as! String
                                    
                                    
                                    
                                    // parse for London or numbers or road or St. which arent shop names
                                    
                                    if (name == "London" || name == "Shoreditch" || name.contains("1") || name.contains("2") || name.contains("3") || name.contains("4") || name.contains("5") || name.contains("6") || name.contains("7") || name.contains("8") || name.contains("9") || name.lowercased().contains("st.") || name.contains("anythingelse...")) {
                                        
                                    } else {
                                        
                                        UserDefaults.standard.set(name, forKey: "lastPlaceVisited")
                                        completion("done")
                                        return
                                    }
                                    
                                   
                                    
                                    
                                    
                        
                                    
                                    
                                    
                                    if let geometry = item["geometry"] as? JSONStandard{
                                        if let location = geometry["location"] as? [String : Any]{
                                            
                                         //   self.latitude = (location["lat"] as? Double)!
                                         //   self.longitude = (location["lng"] as? Double)!
                                        }
                                    }
                                    
                                }
                            }
                        } catch{
                                    print(error)
                                }
                        
                    
                        
                    
                }catch {
                    // print(error.localizedDescription)
                    
                }
                
                
                completion("done")
                
            }
            
        }
        
        task.resume()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        var locationString : String = "0,0"
        let latString = String(format: "%f", locations.last!.coordinate.latitude)
        let lonString = String(format: "%f", locations.last!.coordinate.longitude)
        locationString = latString + "," + lonString
        
        self.lastLocation = locations.last!
        
        /*
        if (Helper.getUserDefaultsInt(name: "bitqueenConsentGiven") == 1) {
            BitqueenHelper.sendLatLon(latLon: locationString)
        }
        */
        
        UserDefaults.standard.set(locationString, forKey: "lastLatLon")
        
        DispatchQueue.main.async {
            let nowTimeStamp = self.getCurrentTimeStamp()
            
            //self.setLocalNotification(title: "Location", body: "changed " + nowTimeStamp, devTime: 0, withSound : false)
            
            // only interested in leaving house and where you are at lunchtime
            let hour = Calendar.current.component(.hour, from: Date())
            
            if (hour < 5 && hour > 11) {
                UserDefaults.standard.set(locationString, forKey: "homeLatLon")
            }
            
            if (hour < 9 && hour > 17) {
               
                // only track 9-6 or.... figure out home on the device and dont send that to the server
                
               //OFF  return
            }
            
            // stop micro movements?
            if (self.lastLocation != nil) {
                let distance : Double = self.lastLocation!.distance(from: locations.last!)
                if (distance < 10.0) {
                    //  return
                }
            }
            
            if (self.justLogged) {
                return
            }
            
            self.justLogged = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
               self.justLogged = false
            }
            
            // google
            AppDelegate.googlePlaces(latLon: locationString) { (output) in
                
            }
            // apple
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
            
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                
                var street : String = "..."
                
                if error != nil {
                    print("Hay un error")
                } else {
                    
                    var placeMark : CLPlacemark!
                    placeMark = placemarks![0]
                    if(placeMark == nil) {
                        street = "?"
                    }
                    
                    // alert
                   // self.showTopLevelAlert(placeName: placeMark.name!)
                    
                    /* https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=51.52621753,-0.08271717&radius=10&key=AIzaSyCOMYmJ87krUcmAYulDb15u6wRw4Jxewvo
                     
                     name
                     
                     "name" : "Ashford City",
                     "place_id"
                     
                    parse for top x places and then show offers,
 */
                    
                    UserDefaults.standard.set(placeMark.name, forKey: "lastPlaceVisited")
                    
                    street = (placeMark.name?.replacingOccurrences(of: "1", with: ""))!
                    street = street.replacingOccurrences(of: "2", with: "")
                    street = street.replacingOccurrences(of: "3", with: "")
                    street = street.replacingOccurrences(of: "4", with: "")
                    street = street.replacingOccurrences(of: "5", with: "")
                    street = street.replacingOccurrences(of: "6", with: "")
                    street = street.replacingOccurrences(of: "7", with: "")
                    street = street.replacingOccurrences(of: "8", with: "")
                    street = street.replacingOccurrences(of: "9", with: "")
                    street = street.replacingOccurrences(of: "0", with: "")
                    
                    if (street.count > 2) {
                        let firstTwo : String = String(street.prefix(2))
                        if (firstTwo == "A " || firstTwo == "B " || firstTwo == "C ") {
                            street = String(street.dropFirst(2))
                        }
                    }
                    if (street.count > 2) {
                        let first : String = String(street.prefix(1))
                        if (first == " ") {
                            street = String(street.dropFirst(1))
                        }
                    }
                    
                    street = street.replacingOccurrences(of: "- ", with: "")
                    street = street.replacingOccurrences(of: " -", with: "")
                    street = street.replacingOccurrences(of: " - ", with: "")
                    street = street.replacingOccurrences(of: "-", with: "")
                    street = street.replacingOccurrences(of: "– ", with: "")
                    street = street.replacingOccurrences(of: " –", with: "")
                    street = street.replacingOccurrences(of: " – ", with: "")
                    street = street.replacingOccurrences(of: "–", with: "")
                    
                    let hour = Calendar.current.component(.hour, from: Date())
                    
                    UserDefaults.standard.set(locationString, forKey: "genericLatLon")
                    UserDefaults.standard.set(locationString, forKey: "lastLatLon")
                    UserDefaults.standard.set(street, forKey: "streetNow")
                    
                    if (hour > 11 && hour < 16) {
                        
                        if (Calendar.current.component(.weekday, from: Date()) == 1 ) {
                            UserDefaults.standard.set(locationString, forKey: "sunLatLon")
                            UserDefaults.standard.set(street, forKey: "streetSunday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 2 ) {
                            UserDefaults.standard.set(locationString, forKey: "monLatLon")
                            UserDefaults.standard.set(street, forKey: "streetMonday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 3 ) {
                            UserDefaults.standard.set(locationString, forKey: "tueLatLon")
                            UserDefaults.standard.set(street, forKey: "streetTuesday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 4 ) {
                            UserDefaults.standard.set(locationString, forKey: "wedLatLon")
                            UserDefaults.standard.set(street, forKey: "streetWednesday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 5 ) {
                            UserDefaults.standard.set(locationString, forKey: "thuLatLon")
                            UserDefaults.standard.set(street, forKey: "streetThursday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 6 ) {
                            UserDefaults.standard.set(locationString, forKey: "friLatLon")
                            UserDefaults.standard.set(street, forKey: "streetFriday")
                        }
                        if (Calendar.current.component(.weekday, from: Date()) == 7 ) {
                            UserDefaults.standard.set(locationString, forKey: "satLatLon")
                            UserDefaults.standard.set(street, forKey: "streetSaturday")
                        }
                        
                    }
                }
                
                
            }
            
        }
    }

}

